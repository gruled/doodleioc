using System.Collections;
using System.Collections.Generic;
using strange.extensions.command.impl;
using UnityEngine;

public class LoadPlayerCommand : Command
{
    public override void Execute()
    {
        var asset = LoadAssetLibrary.instance.player;
        asset.InstantiateAsync();
    }
}
