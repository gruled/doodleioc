using System.Collections;
using System.Collections.Generic;
using strange.extensions.command.impl;
using UnityEngine;

public class PressRightCommand : Command
{
    [Inject] public MoveRightSignal moveRightSignal { get; set; }

    public override void Execute()
    {
        moveRightSignal.Dispatch();
    }
}
