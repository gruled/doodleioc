using System.Collections;
using System.Collections.Generic;
using strange.extensions.command.impl;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class LoadCameraCommand : Command
{
    public override void Execute()
    {
        var asset = LoadAssetLibrary.instance.camera;
        asset.InstantiateAsync();
    }
}
