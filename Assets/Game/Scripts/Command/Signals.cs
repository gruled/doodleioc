using System.Collections;
using System.Collections.Generic;
using strange.extensions.signal.impl;
using UnityEngine;

public class StartSignal : Signal { }

public class MoveLeftSignal : Signal{}
public class MoveRightSignal : Signal{}
public class PressLeftSignal : Signal{}
public class PressRightSignal : Signal{}
public class SpawnObstacleSignal : Signal<Transform>{}