using System.Collections;
using System.Collections.Generic;
using strange.extensions.command.impl;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class LoadControlCommand : Command
{
    public override void Execute()
    {
        AssetReference assetReference = LoadAssetLibrary.instance.control;
        assetReference.InstantiateAsync();
    }
}
