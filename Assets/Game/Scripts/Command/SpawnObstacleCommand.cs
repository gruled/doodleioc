using System.Collections;
using System.Collections.Generic;
using strange.extensions.command.impl;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class SpawnObstacleCommand : Command
{
    [Inject] public Transform parentTransform { get; set; }
    [Inject] public IGameModel gameModel { get; set; }

    public override void Execute()
    {
        AssetReference assetReference = LoadAssetLibrary.instance.obstacle;
        var obstacle = assetReference.InstantiateAsync(parentTransform);
        obstacle.Result.transform.localPosition =
            new Vector3(Random.Range(-2.0f, 2.0f), gameModel.getMaxHeight() + 3.3f, 1.0f);
    }
}
