using System.Collections;
using System.Collections.Generic;
using strange.extensions.command.impl;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class LoadUICommand : Command
{
    public override void Execute()
    {
        AssetReference assetReference = LoadAssetLibrary.instance.ui;
        assetReference.InstantiateAsync();
    }
}
