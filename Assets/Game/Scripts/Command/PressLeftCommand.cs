using System.Collections;
using System.Collections.Generic;
using strange.extensions.command.impl;
using UnityEngine;

public class PressLeftCommand : Command
{
    [Inject] public MoveLeftSignal moveLeftSignal { get; set; }

    public override void Execute()
    {
        moveLeftSignal.Dispatch();
    }
}
