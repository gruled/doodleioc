using System.Collections;
using System.Collections.Generic;
using strange.extensions.command.api;
using strange.extensions.command.impl;
using strange.extensions.context.impl;
using UnityEngine;

public class ObstacleContext : MVCSContext
{
    public ObstacleContext(MonoBehaviour view, bool autoMapping) : base(view, autoMapping)
    {
    }
    
    protected override void addCoreComponents()
    {
        base.addCoreComponents();
        injectionBinder.Unbind<ICommandBinder>();
        injectionBinder.Bind<ICommandBinder>().To<SignalCommandBinder>();
    }

    protected override void mapBindings()
    {
        base.mapBindings();
        mediationBinder.Bind<ObstacleRootView>().To<ObstacleRootMediator>();
        mediationBinder.Bind<ObstacleView>().To<ObstacleMediator>();
        commandBinder.Bind<SpawnObstacleSignal>().To<SpawnObstacleCommand>();
    }
}
