using System;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.context.impl;
using UnityEngine;

public class PlayerRoot : ContextView
{
    private void Awake()
    {
        context = new PlayerContext(this, true);
        context.Start();
    }
}
