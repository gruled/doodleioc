using System;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.context.impl;
using UnityEngine;

public class CameraRoot : ContextView
{
    private void Awake()
    {
        context = new CameraContext(this, true);
        context.Start();
    }
}
