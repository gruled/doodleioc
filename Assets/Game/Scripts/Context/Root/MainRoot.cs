using System;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.context.impl;
using UnityEngine;

public class MainRoot : ContextView
{
    private void Awake()
    {
        context = new MainContext(this, true);
        context.Start();
    }
}
