using System;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.context.impl;
using UnityEngine;

public class ControlRoot : ContextView
{
    private void Awake()
    {
        context = new ControlContext(this, true);
        context.Start();
    }
}
