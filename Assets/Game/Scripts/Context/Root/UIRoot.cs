using System;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.context.impl;
using UnityEngine;

public class UIRoot : ContextView
{
    private void Awake()
    {
        context = new UIContext(this, true);
        context.Start();
    }
}
