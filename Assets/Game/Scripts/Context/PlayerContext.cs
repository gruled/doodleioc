using System.Collections;
using System.Collections.Generic;
using strange.extensions.command.api;
using strange.extensions.command.impl;
using strange.extensions.context.impl;
using UnityEngine;

public class PlayerContext : MVCSContext
{
    public PlayerContext(MonoBehaviour view, bool autoMapping) : base(view, autoMapping)
    {
    }

    protected override void addCoreComponents()
    {
        base.addCoreComponents();
        injectionBinder.Unbind<ICommandBinder>();
        injectionBinder.Bind<ICommandBinder>().To<SignalCommandBinder>();
    }

    protected override void mapBindings()
    {
        base.mapBindings();
        mediationBinder.Bind<PlayerView>().To<PlayerMediator>();
        
        injectionBinder.Bind<IPlayerModel>().To<PlayerModel>().ToSingleton();
        injectionBinder.Bind<MoveLeftSignal>().ToSingleton().CrossContext();
        injectionBinder.Bind<MoveRightSignal>().ToSingleton().CrossContext();
    }
}
