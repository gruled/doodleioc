using System.Collections;
using System.Collections.Generic;
using strange.examples.multiplecontexts.game;
using strange.extensions.command.api;
using strange.extensions.command.impl;
using strange.extensions.context.impl;
using strange.extensions.signal.impl;
using UnityEngine;

public class MainContext : MVCSContext
{
    public MainContext(MonoBehaviour view, bool autoMapping) : base(view, autoMapping)
    {
    }

    protected override void addCoreComponents()
    {
        base.addCoreComponents();
        injectionBinder.Unbind<ICommandBinder>();
        injectionBinder.Bind<ICommandBinder>().To<SignalCommandBinder>();
    }

    protected override void mapBindings()
    {
        base.mapBindings();
        commandBinder.Bind<StartSignal>()
            .To<LoadCameraCommand>()
            .To<LoadPlayerCommand>()
            .To<LoadControlCommand>()
            .To<LoadObstacleRootCommand>()
            .To<LoadUICommand>()
            .InSequence()
            .Once();

        injectionBinder.Bind<IGameModel>().To<GameModel>().ToSingleton().CrossContext();
    }

    public override void Launch()
    {
        base.Launch();
        Signal signal = injectionBinder.GetInstance<StartSignal>();
        signal.Dispatch();
    }
}
