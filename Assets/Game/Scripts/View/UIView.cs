using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.UI;

public class UIView : View
{
    public Text text;
    protected override void Awake()
    {
        base.Awake();
        text = GetComponent<Text>();
    }
}
