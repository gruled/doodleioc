using System;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using UnityEngine;

public class PlayerView : View
{
    public event Action ObstacleEnter2D;
    public Rigidbody2D rigidbody2D;
    public SpriteRenderer spriteRenderer;

    protected override void Awake()
    {
        base.Awake();
        rigidbody2D = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (rigidbody2D.velocity.y<=0)
        {
            ObstacleEnter2D?.Invoke();    
        }
    }
}
