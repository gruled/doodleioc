using System;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using UnityEngine;

public class UIMediator : Mediator
{
    [Inject] public UIView uiView { get; set; }
    [Inject] public IGameModel gameModel { get; set; }

    private void FixedUpdate()
    {
        uiView.text.text = $"Max height: {gameModel.getMaxHeight()}";
    }
}
