using System;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using UnityEngine;

public class CameraMediator : Mediator
{
    [Inject] public CameraView cameraView { get; set; }
    [Inject] public IGameModel gameModel { get; set; }

    private void FixedUpdate()
    {
        cameraView.transform.position = new Vector3(0.0f, gameModel.getMaxHeight(), 0.0f);
    }
}
