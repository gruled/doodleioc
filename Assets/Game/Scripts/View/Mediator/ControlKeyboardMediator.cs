using System;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using UnityEngine;

public class ControlKeyboardMediator : Mediator
{
    [Inject] public ControlKeyboardView controlKeyboardView { get; set; }
    [Inject] public IControlModel controlModel { get; set; }
    // [Inject] public PressLeftSignal pressLeftSignal { get; set; }
    // [Inject] public PressRightSignal pressRightSignal { get; set; }
    [Inject] public MoveLeftSignal MoveLeftSignal { get; set; }
    [Inject] public MoveRightSignal MoveRightSignal { get; set; }

    private void Update()
    {
        // if (Input.GetKey(controlModel.GetLeftKeyCode()))
        // {
        //     pressLeftSignal.Dispatch();
        // }
        // else if(Input.GetKey(controlModel.GetRightKeyCode()))
        // {
        //     pressRightSignal.Dispatch();
        // }
        
        if (Input.GetKey(controlModel.GetLeftKeyCode()))
        {
            MoveLeftSignal.Dispatch();
        }
        else if(Input.GetKey(controlModel.GetRightKeyCode()))
        {
            MoveRightSignal.Dispatch();
        }
    }
}
