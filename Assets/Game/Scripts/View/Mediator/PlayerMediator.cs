using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.AddressableAssets;
using Random = UnityEngine.Random;

public class PlayerMediator : Mediator
{
    [Inject] public PlayerView playerView { get; set; }
    [Inject] public IPlayerModel playerModel { get; set; }
    [Inject] public IGameModel gameModel { get; set; }
    [Inject] public MoveLeftSignal moveLeftSignal { get; set; }
    [Inject] public MoveRightSignal moveRightSignal { get; set; }

    private DoodleSpriteList _doodleSpriteList;

    private void Awake()
    {
        StartCoroutine(_delayedAwake());
        _loadSpriteList();
    }

    private void FixedUpdate()
    {
        float currentHeight = playerView.transform.position.y;
        if (currentHeight > gameModel.getMaxHeight())
        {
            gameModel.setMaxHeight(currentHeight);
        }
    }

    private void _loadSpriteList()
    {
        int rnd = Random.Range(0, SpriteListLibrary.instance.spriteLists.Count);
        _doodleSpriteList = SpriteListLibrary.instance.spriteLists[rnd];
    }

    private IEnumerator _delayedAwake()
    {
        yield return new WaitForSeconds(0.01f);
        playerView.ObstacleEnter2D += Jump;
        moveLeftSignal.AddListener(DoLeft);
        moveRightSignal.AddListener(DoRight);
        playerView.spriteRenderer.sprite = _doodleSpriteList.left;
    }

    private void DoRight()
    {
        playerView.transform.position += Vector3.right * Time.deltaTime * playerModel.GetStrafeForce();
        playerView.spriteRenderer.flipX = true;
    }

    private void DoLeft()
    {
        playerView.transform.position += Vector3.left * Time.deltaTime * playerModel.GetStrafeForce();
        playerView.spriteRenderer.flipX = false;
    }

    private void Jump()
    {
        playerView.rigidbody2D.AddRelativeForce(Vector2.up * playerModel.GetJumpForce(), ForceMode2D.Impulse);
        StopCoroutine(_jumpSprite());
        StartCoroutine(_jumpSprite());
    }

    private IEnumerator _jumpSprite()
    {
        playerView.spriteRenderer.sprite = _doodleSpriteList.leftJumped;
        yield return new WaitForSeconds(0.3f);
        playerView.spriteRenderer.sprite = _doodleSpriteList.left;
    }
}
