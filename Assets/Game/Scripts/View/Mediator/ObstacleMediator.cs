using System;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using UnityEngine;

public class ObstacleMediator : Mediator
{
    [Inject] public ObstacleView obstacleView { get; set; }
    [Inject] public IGameModel gameModel { get; set; }

    private void FixedUpdate()
    {
        if (gameModel.getMaxHeight()-obstacleView.transform.position.y>5.0f)
        {
            DestroyImmediate(obstacleView.gameObject);
        }
    }
}
