using System;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using UnityEngine;

public class ObstacleRootMediator : Mediator
{
    [Inject] public IGameModel gameModel { get; set; }
    [Inject] public SpawnObstacleSignal spawnObstacleSignal { get; set; }
    private float _prevHeight = -1.0f;
    
    private void FixedUpdate()
    {
        float current = gameModel.getMaxHeight();
        if (current-_prevHeight > 3.0f)
        {
            _prevHeight = current;
            spawnObstacleSignal.Dispatch(transform);
        }
    }
}
