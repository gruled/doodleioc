using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DoodleSpriteList", menuName = "Scriptable Objects/ Doodle Sprite List")]
public class DoodleSpriteList : ScriptableObject
{
    [SerializeField] private Sprite _left;
    [SerializeField] private Sprite _leftJumped;

    public Sprite left => _left;

    public Sprite leftJumped => _leftJumped;
}
