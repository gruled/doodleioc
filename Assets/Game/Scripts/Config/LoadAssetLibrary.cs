using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.AddressableAssets;

[CreateAssetMenu(fileName = "LoadAssetLibrary", menuName = "Scriptable Objects/ Load Asset Library", order = 1)]
public class LoadAssetLibrary : ScriptableSingleton<LoadAssetLibrary>
{
    [SerializeField] private AssetReference _camera;
    [SerializeField] private AssetReference _player;
    [SerializeField] private AssetReference _control;
    [SerializeField] private AssetReference _obstacleRoot;
    [SerializeField] private AssetReference _obstacle;
    [SerializeField] private AssetReference _ui;

    public AssetReference camera => _camera;

    public AssetReference player => _player;

    public AssetReference control => _control;

    public AssetReference obstacleRoot => _obstacleRoot;

    public AssetReference obstacle => _obstacle;

    public AssetReference ui => _ui;
}
