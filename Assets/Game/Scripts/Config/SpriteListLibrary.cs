using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "SpriteListLibrary", menuName = "Scriptable Objects/ Sprite List Library")]
public class SpriteListLibrary : ScriptableSingleton<SpriteListLibrary>
{
    [SerializeField] private List<DoodleSpriteList> _spriteLists;

    public List<DoodleSpriteList> spriteLists => _spriteLists;
}
