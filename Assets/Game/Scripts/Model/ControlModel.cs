using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlModel : IControlModel
{
    private KeyCode left = KeyCode.LeftArrow;
    private KeyCode right = KeyCode.RightArrow;

    public KeyCode GetLeftKeyCode()
    {
        return left;
    }

    public KeyCode GetRightKeyCode()
    {
        return right;
    }
}
