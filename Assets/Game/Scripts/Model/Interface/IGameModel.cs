using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGameModel
{
    float getMaxHeight();
    void setMaxHeight(float height);
}
