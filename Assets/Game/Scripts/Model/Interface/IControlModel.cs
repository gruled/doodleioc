using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IControlModel
{
    KeyCode GetLeftKeyCode();
    KeyCode GetRightKeyCode();
}
