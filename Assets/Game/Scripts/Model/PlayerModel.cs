using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerModel : IPlayerModel
{
    private float _jumpForce = 16.0f;
    private float _strafeForce = 3.0f;
    
    public float GetJumpForce()
    {
        return _jumpForce;
    }

    public float GetStrafeForce()
    {
        return _strafeForce;
    }
}
