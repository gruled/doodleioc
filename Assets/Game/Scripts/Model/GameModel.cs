using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameModel : IGameModel
{
    private float maxHeight = 0.0f;
    
    public float getMaxHeight()
    {
        return maxHeight;
    }

    public void setMaxHeight(float height)
    {
        maxHeight = height;
    }
}
